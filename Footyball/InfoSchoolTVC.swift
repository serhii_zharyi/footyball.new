//
//  InfoSchoolTVC.swift
//  Footyball
//
//  Created by Serhii Zharyi on 15.06.17.
//  Copyright © 2017 Serhii Zharyi. All rights reserved.
//

import UIKit

class InfoSchoolTVC: UITableViewController {
    
    let cellText = ["Footyball Ukraine", "Ценности", "Программы", "Расписание тренировок", "Команда тренеров", "Безопасность", "Контакты"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // method to run when table view cell is tapped
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // perform segue with sender
        performSegue(withIdentifier: "showInfo", sender: cellText[indexPath.row])
        //Change the selected background view of the cell.
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInfo" {
            if let detailVC = segue.destination as? InfoSchoolDetailVC {
                detailVC.infoTitle = sender as? String ?? "none"
            }
        }
    }

    
}
