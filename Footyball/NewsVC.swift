//
//  NewsVC.swift
//  Footyball
//
//  Created by Serhii Zharyi on 05.06.17.
//  Copyright © 2017 Serhii Zharyi. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class NewsVC: UIViewController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var newsTableView: UITableView!
    
    // data for newsTableView
    //  var news = [PFObject]()
    let words: [String] = ["one", "two", "three", "four", "five"]
    let colors = [UIColor.blue, UIColor.yellow, UIColor.magenta, UIColor.red, UIColor.brown]
    let cellReuseIdentifier = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        
        // delegate for newsTableView
        newsTableView.delegate = self
        newsTableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkUserLogIn()
        
    }
    
    
    @IBAction func loginBarButtonPressed(_ sender: UIBarButtonItem) {
        let currentUser = PFUser.current()
        if currentUser != nil {
            // Do stuff with the user
            PFUser.logOut()
        } else {
            // Show the signup or login screen
            let logInViewController = PFLogInViewController.init()
            logInViewController.delegate = self
            logInViewController.signUpController?.delegate = self
            self.present(logInViewController, animated: true, completion: nil)
        }
        checkUserLogIn()
    }
    
    // START -> CREATING func's for newsTableView
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.words.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: NewsCell = self.newsTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! NewsCell
        
        cell.myView.backgroundColor = self.colors[indexPath.row]
        cell.myCellLabel.text = self.words[indexPath.row]
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        // check if
        performSegue(withIdentifier: "showNewsDetail", sender: words[indexPath.row])
        //Change the selected background view of the cell.
        tableView.deselectRow(at: indexPath, animated: true)
    }
    // -> END
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepare for segue:")
        if segue.identifier == "showNewsDetail" {
            if let detailVC = segue.destination as? NewsDetailVC {
                detailVC.newsHeader = sender as? String ?? ""
            }
        }
    }
    
    func checkUserLogIn() {
        if PFUser.current() != nil {
            navigationItem.rightBarButtonItem?.title = "Log out"
        }
        else {
            navigationItem.rightBarButtonItem?.title = "Login"
        }
    }
    
    func log(_ logInController: PFLogInViewController, didLogIn user: PFUser) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func signUpViewController(_ signUpController: PFSignUpViewController, didSignUp user: PFUser) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //  hide keyboard after press "return"
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //  hide keyboard after click outside keyboard and textField
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
