//
//  NewsDetailVC.swift
//  Footyball
//
//  Created by Ivan Vasilevich on 6/12/17.
//  Copyright © 2017 Serhii Zharyi. All rights reserved.
//

import UIKit
import Parse

class NewsDetailVC: UIViewController {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var mainText: UITextView!
    
    var newsHeader = ""
    var newsFromParse = "".components(separatedBy: " ")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        headerLabel.text = newsHeader
        let query = PFQuery(className: "News")
        query.whereKeyExists("text")
        query.findObjectsInBackground(block: { (objects, error) in
            if let objects = objects {
                print(objects)
                self.newsFromParse = objects.map({ (obj) -> String in
                    obj["text"] as! String
                })
                self.mainText.text = self.newsFromParse[2] // don't know what Index to put for self.newsFromParse[_??_]
            }
            else {
                print(error!)
            }
        })
        
    }
    
}
