//
//  SaveNewUserVC.swift
//  Footyball
//
//  Created by Serhii Zharyi on 01.06.17.
//  Copyright © 2017 Serhii Zharyi. All rights reserved.
//

import UIKit
import Parse

class SaveNewUserVC: UIViewController {

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func createAccount(_ sender: UIButton) {
        let userLogin = loginField.text
        let userEmail = emailField.text
        let userPassword = passwordField.text
        let userRepeatPassword = confirmPasswordField.text
        if ((userLogin?.isEmpty)! || (userEmail?.isEmpty)! || (userPassword?.isEmpty)!) {
            // alert message
            displayAlertMessage(userMessage: "All fields are required")
            return
        }
        if (userPassword != userRepeatPassword) {
            // alert message
            displayAlertMessage(userMessage: "Passwords do not match")
            return
        }
        // Store data example
        let newUser = PFObject(className: "Users")
        newUser["login"] = userLogin
        newUser["email"] = userEmail
        newUser["password"] = userPassword
        newUser.saveInBackground { (success:Bool, error:Error?) -> Void in
            if success {
                print("user saved")
            } else {
                print(error ?? "some error")
            }
        }
        // go to the main page of App
        performSegue(withIdentifier: "mainPage", sender: nil)
        
    }
    
    func displayAlertMessage(userMessage: String) {
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }

}
