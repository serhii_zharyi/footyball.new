//
//  ProfileVC.swift
//  Footyball
//
//  Created by Serhii Zharyi on 16.06.17.
//  Copyright © 2017 Serhii Zharyi. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var honorsTableView: UITableView!
    
    let cellReuseIdentifier = "honorsCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        honorsTableView.delegate = self
        honorsTableView.dataSource = self
    }

    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // количество пройденных аттестаций Футболистом
        
        return 5 // поменять
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ProfileHonorsCell = self.honorsTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ProfileHonorsCell
        
        cell.numberOfStep.text = "first"
        
        return cell
    }


}
