//
//  LoginVC.swift
//  Footyball
//
//  Created by Serhii Zharyi on 31.05.17.
//  Copyright © 2017 Serhii Zharyi. All rights reserved.
//

import UIKit
import Parse

class LoginVC: UIViewController {

    @IBOutlet weak var loginToCheck: UITextField!
    @IBOutlet weak var passwordToCheck: UITextField!
    
    var login = "".components(separatedBy: " ")
    var password = ""
    var email = ""
    var confirmEmail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let query = PFQuery(className: "Users")
        query.whereKeyExists("login")
        query.findObjectsInBackground { (objects: [PFObject]?, error: Error?) -> Void in
            if let objectsAll = objects {
                print(objectsAll)
                self.login = objectsAll.map({ (obj) -> String in
                    obj["login"] as! String
                })
                
//                login = objects.map({ (obj) -> String in
//                    obj["login"] as! String
//                })
                
            } else {
                print(error ?? "some error")
            }
        }

    }
    
    @IBAction func signIn(_ sender: UIButton) {
        // check does user exist in parse.back4app.database
    }
    
    @IBAction func createAccount(_ sender: UIButton) {
        performSegue(withIdentifier: "createAccount", sender: nil)
    }
    
}
