//
//  GalleryCollectionViewCell.swift
//  Footyball
//
//  Created by Serhii Zharyi on 16.06.17.
//  Copyright © 2017 Serhii Zharyi. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoForGallery: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
